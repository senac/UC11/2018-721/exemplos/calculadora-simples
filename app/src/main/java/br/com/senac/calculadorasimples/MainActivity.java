package br.com.senac.calculadorasimples;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textViewDisplay;
    private EditText editTextOperando1;
    private EditText editTextOperando2;
    private Button btnSoma;
    private Button btnSubtracao;
    private Button btnMultiplicacao;
    private Button btnDivisao;

    private final int SOMA = 1;
    private final int SUBTRACAO = 2;
    private final int MULTIPLICACAO = 3;
    private final int DIVISAO = 4;


    private void initComponentes() {
        textViewDisplay = (TextView) findViewById(R.id.display);
        editTextOperando1 = (EditText) findViewById(R.id.operando1);
        editTextOperando2 = (EditText) findViewById(R.id.operando2);
        btnSoma = (Button) findViewById(R.id.btnSoma);
        btnSubtracao = (Button) findViewById(R.id.btnSubtracao);
        btnMultiplicacao = (Button) findViewById(R.id.btnMultiplicacao);
        btnDivisao = (Button) findViewById(R.id.btnDivisao);

        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular(SOMA);
            }
        });
        btnSubtracao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular(SUBTRACAO);
            }
        });
        btnMultiplicacao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular(MULTIPLICACAO);
            }
        });
        btnDivisao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                calcular(DIVISAO);
            }
        });

    }


    private void calcular(int operacao) {
        editTextOperando1.onEditorAction(EditorInfo.IME_ACTION_DONE);
        editTextOperando2.onEditorAction(EditorInfo.IME_ACTION_DONE);
        double op1 = Double.parseDouble(editTextOperando1.getText().toString());
        double op2 = Double.parseDouble(editTextOperando2.getText().toString());

        double resultado = 0;
        switch (operacao) {
            case SOMA:
                resultado = op1 + op2;
                break;
            case SUBTRACAO:
                resultado = op1 - op2;
                break;
            case MULTIPLICACAO:
                resultado = op1 * op2;
                break;
            case DIVISAO:
                resultado = op1 / op2;
                break;

        }


        textViewDisplay.setText(String.valueOf(resultado));
        editTextOperando1.setText("");
        editTextOperando2.setText("");
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initComponentes();


    }
}
